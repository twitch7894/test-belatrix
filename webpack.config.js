const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpackMerge = require("webpack-merge");
const path = require("path");
const loadPresets = require("./build-utils/loadPresets");
const OpenBrowserPlugin = require("open-browser-webpack-plugin");
const modeConfig = env => require(`./build-utils/webpack.${env.mode}.js`)(env);
const APP_DIR = path.resolve(__dirname, "./src"); // <===== new stuff added here

module.exports = ({ mode, presets } = { mode: "production", presets: [] }) => {
  console.log(mode);
  return webpackMerge(
    {
      mode,
      entry: ["@babel/polyfill", APP_DIR], // <===== new stuff added here
      module: {
        rules: [
          {
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: {
              loader: "babel-loader",
              options: {
                presets: ["@babel/preset-env", "@babel/preset-react"]
              }
            }
          },
          {
            test: /\.(jpg|png|gif|svg)$/,
            use: {
              loader: "url-loader",
              options: {
                limit: 10000,
                fallback: "file-loader",
                name: "assets/images/[name].[hash].[ext]"
              }
            }
          }
        ]
      },
      plugins: [
        new webpack.ProgressPlugin(),
        new HtmlWebpackPlugin({
          template: "./src/index.html",
          filename: "./index.html"
        }),
        new OpenBrowserPlugin({ url: "http://localhost:8080" })
      ]
    },
    modeConfig({ mode, presets }),
    loadPresets({ mode, presets })
  );
};
