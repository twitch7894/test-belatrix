const CompressionWebpackPlugin = require("compression-webpack-plugin");

module.exports = () => ({
  plugins: [new CompressionWebpackPlugin({
    filename: "[path].gz[query]",
        algorithm: "gzip",
        test: /\.(js|css)$/,
  })]
});
