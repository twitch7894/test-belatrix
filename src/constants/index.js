/**
 * GET DATA FIXER
 */

const prefix = "DATA_FIXER/";

export const FETCHING = `${prefix}FETCHING`;
export const SUCCESS = `${prefix}SUCCESS`;
export const ERROR = `${prefix}ERROR`;
