const axios = require("axios");

test("the data is peanut butter", async () => {
  const res = await axios.get(
    "http://data.fixer.io/api/latest?access_key=33b23d6e01efe285daf21f65e1124757"
  );
  let { data } = res;
  expect(data).not.toBeUndefined();
});
