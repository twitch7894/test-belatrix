import React from "react";
import { render } from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./styles/index.scss";
import Roster from "./router";
import { BrowserRouter } from "react-router-dom";

render(
  <BrowserRouter>
    <Roster />
  </BrowserRouter>,
  document.getElementById("root")
);
