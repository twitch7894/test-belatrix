/**
 *  AVATAR DATA FIXER
 */
import React from "react";

const Avatar = ({ Img }) => {
  return (
    <div className="avatar">
      <img src={Img} alt="avatar the data fixer" />
    </div>
  );
};

export default Avatar;
