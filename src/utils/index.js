/**
 *
 * @param {number} num
 */

// Format Number
export const format_number = num => {
  return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};
