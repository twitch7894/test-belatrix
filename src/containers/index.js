import React from "react";
import Header from "../components/header";
import Content from "../components/content";

function Home() {
  return (
    <React.Fragment>
      <Header />
      <Content />
    </React.Fragment>
  );
}

export default Home;
