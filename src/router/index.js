import React from "react";
import { Switch, Route, Router } from "react-router-dom";
import Home from "../containers";
import NotFound from "../components/NotFound";

const Roster = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route component={NotFound} />
  </Switch>
);

export default Roster;
