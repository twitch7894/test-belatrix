/**
 * ENDPOINTS
 */

// ENDPOINT DATA FIXER
const URL = "http://data.fixer.io/api/latest?access_key=";
const KEY = "33b23d6e01efe285daf21f65e1124757";

// ENDPOINT IMG AVATAR
const URL_IMAGE = "https://i.pravatar.cc/150";

export const ENDPOINTS = {
  DATA_FIXER: `${URL}${KEY}`,
  AVATAR: URL_IMAGE
};
