import React, { useState, useEffect } from "react";
import useApiRequest from "../data/api";
import { SUCCESS, ERROR } from "../constants";
import { ENDPOINTS } from "../config/endPoints";
import { Link } from 'react-router-dom';
import { format_number } from "../utils";

export const ApiRequest = () => {
  const [number, setNumber] = useState("");
  const [changemonkey, setChangemonkey] = useState(0);
  const [show, setShow] = useState(false);
  const [notfound, setNotfound] = useState(false);
  const [disabled, setDisabled] = useState(true);
  const [{ status, response }, makeRequest] = useApiRequest(
    ENDPOINTS.DATA_FIXER,
    {
      verb: "get"
    }
  );
  useEffect(() => {
    if (status === SUCCESS && show === true) {
      if (
        typeof response.data.rates === "object" &&
        response.data.rates.USD !== undefined
      ) {
        let change = format_number(number * response.data.rates.USD);
        setChangemonkey(change);
        setShow(false);
      } else {
        setNotfound(true);
      }
    }
  });
  //let data =  format_number(number)
  return (
    <div className={`${notfound === false ? "content" : "content-error"}`}>
      {status !== ERROR && notfound === false ? (
        <React.Fragment>
          <div className="content_input">
            <div className="content_EUR">
              <div className="input-group-prepend">
                <span className="input-group-text">EUR</span>
              </div>
              <input
                type="number"
                pattern="[0-9]*"
                className="form-control"
                value={number}
                onBlur={() => setShow(!show)}
                placeholder="Ingrese el Monto"
                onChange={e => setNumber(e.target.value)}
              />
            </div>
            <div className="content_USD">
              <div className="input-group-prepend">
                <span className="input-group-text">USD</span>
              </div>
              <input
                disabled={disabled}
                type="string"
                className="form-control"
                value={changemonkey}
              />
            </div>
          </div>
          <div className="button-calculate">
            <button className="calculate" onClick={makeRequest}>
              Calculate
            </button>
          </div>
        </React.Fragment>
      ) : (
        <div className="Error">
          <p className="Error_text">An error has occurred</p>
            <a  className="Error_button" href="/">Reset</a>
        </div>
      )}
    </div>
  );
};

export default ApiRequest;
