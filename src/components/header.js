import React from "react";
import Avatar from "../common/avatar";
import { ENDPOINTS } from "../config/endPoints";

function Header() {
  return (
    <React.Fragment>
      <div className="header">
        <Avatar Img={ENDPOINTS.AVATAR} />
      </div>
      <h1 className="title">Money Xchange</h1>
    </React.Fragment>
  );
}

export default Header;
