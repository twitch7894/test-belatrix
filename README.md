## Quick Overview

```sh
git clone https://gitlab.com/twitch7894/test-belatrix
cd test-belatrix
npm install o yarn
```

Then open [http://localhost:8080/](http://localhost:8080/) to see your app.<br>
when you’re ready to deploy to development, bundle with `yarn dev`.
when you’re ready to deploy to production, bundle with `yarn prod`.